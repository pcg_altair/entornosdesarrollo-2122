import java.util.Scanner;

public class Ejemplo1 {

	public static void main(String[] args) {
		// long hay que a�adirle una l al final
		Long edad = 419999999999999999l;
		Integer entero1 = 16666;
		
		// Variables decimales
		Float decimal1 = 6.3f;
		Double decimalDouble = 6.3;

		// Variables booleanas
		Boolean falta = true;
		boolean asiste = false;
		
		// Variable para guardar cadenas de texto
		String nombre = "";
		String asignatura = "Entorno de Desarrollo";
		
		// Variables para guardar caracteres
		Character letraDni = 'D';
		char letraClase = 'A';

		edad = 24l;
		
		/*System.out.println("Buenos d�as introduzca su nombre:");
		Scanner scan = new Scanner(System.in);
		scan.next();//solo lee hasta el primer espacio
		System.out.println("Hola " + nombre);
		
		System.out.println("Introduzca el nombre completo:");
		
		Scanner scan2 = new Scanner(System.in);
		String nombreCompleto = scan2.nextLine();
		System.out.println("Hola " + nombreCompleto);
		
		System.out.println("Introduzca su edad");
		Scanner scan3 = new Scanner(System.in);
		Integer edadPersona = scan3.nextInt();
		System.out.println("Tu edad es " + edadPersona);*/
		
		Integer numeroEntero = 10;
		Double numeroDecimal = (double) numeroEntero;
		System.out.println(numeroDecimal);
		
		double peso = 74.25;
		Integer pesoEntero = (int) peso;
		System.out.println(pesoEntero);
		
		String pesoTexto = String.valueOf(peso);
		String pesoEnteroTexto = String.valueOf(pesoEntero);
		
		System.out.println(pesoTexto);
		
		
		final Double PI = 3.145497984;
		System.out.println(PI);
		
	}

}
