import java.util.Scanner;

public class EjemploCalculadoraIf {

	public static void main(String[] args) {
		
		System.out.println("Introduzca un n�mero:");
		Scanner scan = new Scanner(System.in);
		Double numero1 = scan.nextDouble();
		
		System.out.println("Introduzca otro n�mero:");
		Double numero2 = scan.nextDouble();
		
		System.out.println("�Qu� operaci�n desea realizar? [+, -, *, /, %]");
		String operacion = scan.next();
		
		if (operacion.equals("+")) {
			System.out.println(numero1 + numero2);
		} else if (operacion.equals("-")) {
			System.out.println(numero1 - numero2);
		} else if (operacion.equals("*")) {
			System.out.println(numero1 * numero2);
		} else if (operacion.equals("/")) {
			System.out.println(numero1 / numero2);
		} else if (operacion.equals("%")) {
			System.out.println(numero1 % numero2);
		} else {
			System.out.println("Operaci�n no v�lida");
		}
		
	}

}
