import java.util.Scanner;

public class EjemplosSentenciaIf {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Introduzca su nombre de usuario");
		String nombre = scan.nextLine();
		
		//.equals() => evalua que el valor es el mismo
		//== => evalua es la misma variable y por lo tanto est� en el mismo espacio de
		// memoria
		if (nombre.equals("Pablo")) {
			System.out.println("Eres un profesor");
		} else if(nombre.equals("Francisco")) {
			System.out.println("Eres un alumno");
		} else {
			System.out.println("No s� quien eres");
		}
	}

}
