import java.util.Scanner;

public class Repaso {

	public static void main(String[] args) {
		
		// Numeros enteros
		long numeroLong = 1269769468999999L;
		Long numeroLong2 = 13489798794646464l;
		
		int edad = 56;
		Integer edad2 = 62;
		Integer diaSemana;
		
		diaSemana = 3;
		
		// Numeros decimales
		double temperatura = 24.5;
		Double estatura = 1.76;
		
		float temperaturaFloat = 24.5f;
		Float temperaturaFloat2 = 24.5f;
		
		
		// Cadena de caracteres
		String nombre = "Pablo Carrillo";
		
		// Caracteres
		char letraDni = 'P'; //
		Character letraDni2 = 'T';
		
		letraDni = 'W'; // 87
		//Si sumamos un char o un Character a un int o Integer
		//va a sumar el valor que tiene el char o Character al n�mero entero
		Integer sumaCharacter = 25 + letraDni;// 25 + (el valor asociado a la W)
		System.out.println("Suma char:" + sumaCharacter);
		
		String dniCompleto = "12345678" + letraDni;
		System.out.println("Dni completo:" + dniCompleto);
		
		// Booleanos
		Boolean mayorEdad = true;
		boolean menorEdad = false;
		
		// Imprimir por pantalla
		System.out.println(letraDni);
		
		// Escanear por pantalla
		Scanner scan = new Scanner(System.in);
		System.out.println("Introduzca el n�mero de DNI");
		int numeroDni = scan.nextInt();
		// Concatenar un entero y un char
		// tengo que realizar un casting (convertir un tipo en otro)
		System.out.println(String.valueOf(true) + letraDni);
		//String.valueOf(variableDelTipoQueSea) => lo va a convertir a String
		
		// Creaci�n de una constante
		final Double PI = 3.145982396;
		
		//No se puede modificar su valor => NO => PI = 4.6941;
	}

}
