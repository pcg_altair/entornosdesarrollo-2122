import java.util.Scanner;

public class Utilidades {

	public static void main(String[] args) {
		//Ejercicio 1 y 2
		String saludo = "Hola mundo";
		System.out.println(saludo);
		
		// Ejercicio 3
		Integer numero1 = 60;
		Integer numero2 = 50;
		Integer suma = numero1 + numero2;
		
		System.out.println(suma);
		
		Boolean numero1Mayor = numero1 > numero2;
		System.out.println(numero1Mayor);
		
		Double decimal1 = 2.5;
		Double decimal2 = 5.9;
		
		System.out.println(decimal1 * decimal2);
		
		Scanner scan = new Scanner(System.in);
		System.out.println("�Cu�l es tu nombre?");
		String miNombre = scan.nextLine();
		
		System.out.println("Hola " + miNombre);
		
		System.out.println("Introduzca el n�mero1");
		Integer numeroLectura1 = scan.nextInt();
		
		System.out.println("Introduzca el n�mero 2");
		Integer numeroLectura2 = scan.nextInt();
		Integer suma2 = numeroLectura1 + numeroLectura2;
		System.out.println("La suma de los n�meros es " + suma2);
		
		System.out.println("Adi�s " + miNombre);
	}

}
